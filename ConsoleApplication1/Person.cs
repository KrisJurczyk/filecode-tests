﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApplication1//zmieeeennnn na ładne
{
    public class Person
    {
        public Person()//ctor+tab empty constr
        {
        }
        public Person(string personString)//konstr
        {
            var regex = new Regex("(?<FirstName>.*);(?<LastName>.*);(?<Email>.*);(?<Phone>.*)");//przy pomocy Regex ReSharper->Tools->Validate reg
            var match = regex.Match(personString);
            FirstName = match.Groups["FirstName"].Value;
            LastName = match.Groups["LastName"].Value;
            Email = match.Groups["Email"].Value;
            Phone = match.Groups["Phone"].Value;
            //var person = personString.Split(';');//cięcie stringa po ;
            //FirstName = person[0];
            //LastName = person[1];
            //Email = person[2];
            //Phone = person[3];
        }

        public string FirstName { get; set; }

//proper

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public override string ToString()
        {
            return $"{FirstName};{LastName};{Email};{Phone}";//skleja w string
        }

        protected bool Equals(Person other)
        {
            return string.Equals(FirstName, other.FirstName) && string.Equals(LastName, other.LastName) && string.Equals(Email, other.Email) && string.Equals(Phone, other.Phone);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Person) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (FirstName != null ? FirstName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (LastName != null ? LastName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Email != null ? Email.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Phone != null ? Phone.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}
