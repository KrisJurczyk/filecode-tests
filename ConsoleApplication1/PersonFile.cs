﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ConsoleApplication1
{
    public class PersonFile
    {
        private readonly string _path;

        public PersonFile(string path)
        {
            _path = path;
        }

        public void Save(IEnumerable<Person> persons)
        {
            using (var file = File.Open(_path, FileMode.Create, FileAccess.Write))
            {
                using (var s = new StreamWriter(file))
                {
                    // robimy petle dla person, segregujemy w lambda wg nazwiska, mamy w * wielkość liter
                    foreach (var person in persons.OrderBy(person => person.LastName, StringComparer.OrdinalIgnoreCase))
                    {
                        s.WriteLine(person);
                    }
                }
            }
        }

        //public IEnumerable<Person> Load()
        //{
        //    tworzymy nowa liste
        //       var result = new List<Person>();
        //    using (var file = File.Open(_path, FileMode.Open, FileAccess.Read))
        //    {
        //        using (var s = new StreamReader(file))
        //        {
        //            string line;
        //            while ((line = s.ReadLine()) != null)
        //            {
        //                var person = new Person(line);
        //                result.Add(person);
        //            }
        //            return result;
        //        }
        //    }
        //}

        public Person[] Load()
        {
            // person to takie proper, przyjmujemy je w lambda w tym przypadku
            return LazyLoad().OrderBy(person => person.LastName, StringComparer.OrdinalIgnoreCase).ToArray();
        }

        public IEnumerable<Person> LazyLoad()
        {
            using (var file = File.Open(_path, FileMode.Open, FileAccess.Read))
            {
                using (var s = new StreamReader(file))
                {
                    string line;
                    while ((line = s.ReadLine()) != null)
                    {
                         yield return new Person(line);
                    }
                }
            }
        }
    }
}
